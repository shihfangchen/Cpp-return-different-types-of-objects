#include <opencv2/opencv.hpp>
#include <tuple>
#include <iostream>
#include <string>
#include <stdexcept>

using namespace cv;
using namespace std;

std::tuple<float, String> get_student(float roi, String mode)
{
	if (mode == "str") return std::make_tuple(NULL, "str");
	if (mode == "int") return std::make_tuple(2.9, "");
	throw std::invalid_argument("id");
}

int main()
{
	//Mat roi;
	float ff;
	String mode;
	std::tie(ff, mode) = get_student(1.0, "str");
	std::cout << "str, " << "mode: " << mode << '\n';

	std::tie(ff, mode) = get_student(2.0, "int");
	std::cout << "int, " << "ff: " << ff << '\n';

	system("pause");
}