#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

typedef enum {TYPE_INT,TYPE_STRING}UserDefineType;

typedef struct foo{
    UserDefineType Type;
    union {
        int x;
        char s[256];
    }Value;
    foo(int a=0){Value.x=a; Type = TYPE_INT;}
    foo(char* str){strcpy(Value.s,str); Type = TYPE_STRING;}
    foo(const char* str){strcpy(Value.s,str); Type = TYPE_STRING;}
}StructType;

StructType gStruct;

ostream &operator<<(ostream &s, StructType st) { 
    switch(st.Type)
    {
        case TYPE_INT:
            return s<<st.Value.x;
        case TYPE_STRING:
            return s<< " \"" << st.Value.s << "\" ";
    }
} 

int main()
{
    gStruct = StructType("ASD");
    cout<<gStruct<<endl;
    gStruct = StructType(8975);
    cout<<gStruct<<endl;
    
    cout<<StructType("87999879")<<StructType(875465)<<endl;
    return 0;
}

